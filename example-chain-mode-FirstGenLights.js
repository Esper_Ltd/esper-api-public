const EsperFactory = require('./esper-api');
let esper = new EsperFactory();


// This script is written for compatibility with Gen 1 lights - Round Circular front.
// Running the script will automatically trigger a configure and arm sequence of the Gen1 lights into OLAT mode.
// The modelling light must not be adjusted while this script is running.
// To exit OLAT mode, the lights must be reset, so data will need to be re-uploaded after quitting the script.

initUserInput();

/**
 * Duration in milliseconds to flash one at a time.
 * @type {number}
 */
let userFlashDuration = 30;  // min: 1, max: 30   [mS]

// set to something low while doing initial tests. Increase only when everything has been verified.
let userFlashBrightness = 5;
let focusDurationMillis = 500;
let shutterCaptureDurMillis = 1000;
let numStagesToCapture = 5;

esper.connect()
    .then(() => {
       esper.configureChainModeGen1(userFlashDuration);
       esper.armChainModeGen1(userFlashBrightness);
       promptUser("Press f to test a single flash..." +
           "\n\t k to reset the lights to sequence point zero..." +
           "\n\t K to reset the lights to sequence point zero using a global command..." +
           "\n\t c to capture for [" + shutterCaptureDurMillis + "] milliseconds..." +
           "\n\t C to capture [" + numStagesToCapture + "] stages..." +
           "\n\t x to exit ... \n"
       );
    });

/**
 *
 * @param {String} arg
 */
function promptUser(arg){
    process.stdout.write(arg + " >> ");
}

process.stdin.on("keypress", (err, key) => {
    let char = key.sequence;
    switch (char){
        case "f":
            esper.testFlash();
            break;

        case "k":
            esper.rewindChainModeGen1();
            break;

        case "K":
            esper.useBackendTerminalCommand("k");
            break;

        case "c":
            esper.useBackendTerminalCommand("f1");
            setTimeout(()=>{
                esper.useBackendTerminalCommand("sdur " + shutterCaptureDurMillis);
                setTimeout(()=>{
                    esper.useBackendTerminalCommand("f0");
                }, shutterCaptureDurMillis * 1.25)
            }, focusDurationMillis);
        break;

        case "C":
            esper.useBackendTerminalCommand("f1");
            setTimeout(()=>{
                esper.useBackendTerminalCommand(
                    "dslr-n " + (numStagesToCapture*200) + " " + numStagesToCapture
                    );
                setTimeout(()=>{
                    esper.useBackendTerminalCommand("f0");
                }, numStagesToCapture * 200);
            }, focusDurationMillis);
            break;

        case "x":
            esper.exitChainModeGen1(()=>{
                quit();
            })
            break;
    }
});

function quit(){
    esper.disconnect();
    process.exit(0);
}

function initUserInput(){
    try{
        require("readline").emitKeypressEvents(process.stdin);
        process.stdin.setRawMode(true);
        process.stdout.cursorTo(0,0, ()=>{
            process.stdout.clearScreenDown(()=>{
                process.stdout.write("Started CLI applet. Attempting to connect to controlSuite...");
            });
        })
    }
    catch(err){
        console.error("Couldn't initialise user input... further prompts will be unresponsive.")
    }
}
