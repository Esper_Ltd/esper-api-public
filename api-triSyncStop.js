const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

esper.connect().then(()=>{
    setTimeout(() => {
         esper.useBackendTerminalCommand("tscg");   // Tri Sync Close Gate
        esper.disconnect();
        process.exit(0);
    }, 500);

})
.catch((errs)=>{
    console.error("API::caught errors:");
    console.table(errs);
    console.log(errs);
});
