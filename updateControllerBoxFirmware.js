const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

const fs = require("fs");
const child_process = require("child_process");
const firmwareFile = "ControllerBoxFirmware.hex"
const __PORT = "COM6";

let avrdudeCommand = `avrdude -b115200 -cavr911 -p x64a3u -v -Uflash:w:ControllerBoxFirmware-tsm.hex:i  -e -PCOM3`

//goAvrdude();

function goAvrdude(){
    console.log("\##### RUNNING AVRDUDE #####\n")
    let pc = child_process.exec(avrdudeCommand, (err, stdout, stderr)=>{
        if (err){ console.log("AVR CALL ERR:" + err); }
        if (stderr){
            let lines = stderr.split("\r\n");
            for (let line of lines){
                console.log("AVRDUDE::" + line);
            }
        }
    });
}

let connectTimeout = setTimeout(()=>{
        console.log("ERR: couldn't connect to controlSuite...");
        process.exit(-10);
    },
    1000);
esper.connect()
    .then(()=> {
        console.log("Cancelling Timeout");
        clearTimeout(connectTimeout);
            testAVRDUDE(()=>{
                updateControllerBoxFirmware();
            })
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
});

function updateControllerBoxFirmware(){
    console.log("\n##### updating cb firmware... #####\n");
    esper.useBackendTerminalCommand("cbsrbl");
    setTimeout(()=>{
        esper.useBackendTerminalCommand("cbcs");
        setTimeout(()=>{
            goAvrdude();
        }, 1500);
    }, 1000);
}



function testAVRDUDE(onFound){
    console.log("\n#####   TESTING FOR AVRDUDE   #####\n");
    child_process.exec("avrdude", (err, stdout, stderr)=>{
        if (err){
            console.log(err);
        }
        if (stdout){
            console.log(stdout);
        }
        if (stderr){
            let lines = stderr.split("\r\n");
            let found = false;
            let version = "";
            for (let line of lines){
                if (line.startsWith("avrdude version")){
                    found = true;
                    version = line.split(" ")[2];
                }
                console.log("AVRDUDE :: " + line);
            }
            if (found){
                console.log("\n##### FOUND AVRDUDE, version " + version + " #####\n");
                onFound();
            }
            else{
                console.log("ERR: couldn't verify avrdude is in the PATH");
            }
        }
    });
}
