let esperClass = require("./esper-api");
let esper = new esperClass(true); // enable verbose reporting

// USER CONSTANTS:
const NEUTRAL_LED_ARRAY = [0, 100, 0]
const CROSS_LED_ARRAY = [100, 0, 0]
const OFF_LED_ARRAY = [0, 0, 0]
const flashDurationMillis = 5;  // min 1, max 30
const numLights = 6;
const secondsBetweenTakes = 10;

esper.connect()
    // LED : CROSS
    //////////////
    .then(()=>{
        return esper.globalModellingLight([0, 0, 0]);
    })

    .then(() => {
        let lightSequence = buildLightSequence(CROSS_LED_ARRAY);
        return esper.sequence(lightSequence)
    })

    .then(()=>{
        console.log("Triggering...");
        esper.trigger({
            start:0,
            stages:numLights,
            fps: 3,  // Best to err on the slow side.
                    // This value has no effect on capture rate, it only affect controllerBox timeout
            preFocusDelay: 1000,
            captureMode: 'dslr'})
    })

    .then(()=>{
        return esper.wait(secondsBetweenTakes*1000); // 10 seconds
    })
    // LED : NEUTRAL
    ////////////////
    .then(()=>{
        return esper.globalModellingLight([0, 0, 0]);
    })

    .then(() => {
        let lightSequence = buildLightSequence(NEUTRAL_LED_ARRAY);
        return esper.sequence(lightSequence)
    })

    .then(()=>{
        console.log("Triggering...");
        return esper.trigger({
            start:0,
            stages:numLights,
            fps: 3,  // Best to err on the slow side.
                    // This value has no effect on capture rate, it only affect controllerBox timeout
            preFocusDelay: 1000,
            captureMode: 'dslr'})
    })
    /// Done capturing


    .then(()=>{
        return esper.disconnect();
    })

    .catch((err)=>{
        esper.describeErrors(err);
        esper.disconnect();
    });


function buildLightSequence(ledArrayPattern){
    let toRet = [];
    for (let i = 1; i < 32+1; i++){ // build a sequence of length 32 to ensure old data is overwritten
        let thisStage = [];
        for (let s = 1; s < numLights+1; s++){
            // assume the light should not flash...
            let mfStageObject = {id: s, intensities: OFF_LED_ARRAY, duration: flashDurationMillis};
            if (s === i){ // Unless the light's ID matches the current thisStage index
                mfStageObject.intensities = ledArrayPattern; //
            }
            thisStage.push(mfStageObject);
        }
        toRet.push(thisStage);
    }
    return toRet;
}
