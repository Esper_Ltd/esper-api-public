console.time("Program Start");
const SerialPort = require("serialport");
const ByteLength = SerialPort.parsers.ByteLength;
let serialByteGetter;

let parsedLine = "";

let com = new SerialPort(
    "COM10",
    {baudRate:57600},
    (err)=>{
        if (err){
            console.log("ERR:");
            console.log(err);
        }
        serialByteGetter = com.pipe(new ByteLength({
            length: 1
        }));
        serialByteGetter.on('data', (data) => {
            let byteToBufferBack = data.readUInt8();
            //Helper.print("B.B. < " + byteToBufferBack);
            parseByte(byteToBufferBack);
        });
    }
);

function lineComplete(){
 //   console.log(parsedLine);
    if (parsedLine.startsWith("#")){
        if (parsedLine.length > 13){
            // #TCTM=12040601
            let hours   = Number.parseInt(parsedLine.substr(6,2));
            let minutes = Number.parseInt(parsedLine.substr(8,2));
            let seconds = Number.parseInt(parsedLine.substr(10,2));
            let frames  = Number.parseInt(parsedLine.substr(12,2));
            console.log(hours+","+minutes+","+seconds+","+frames+",");
        }
    }
}

function parseByte(btbb){
    let char = String.fromCharCode(btbb);
    if (char === '#'){
        parsedLine = "";
    }
    if (char === "\n"){
        lineComplete();
    }
    parsedLine += char;

}




