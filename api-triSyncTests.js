const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

esper.connect().then(()=>{
    esper.useBackendTerminalCommand("cbsr");
    setTimeout(()=> {
        esper.useBackendTerminalCommand("PT 0");       // disable simple pass-thru from input jack.
        setTimeout(()=>{
            esper.useBackendTerminalCommand("tssssccttl"); // set sync source to cc/ttl input (3.5mm)
            //z esper.useBackendTerminalCommand("tssssbnc"); // set sync source to bnc
            esper.useBackendTerminalCommand("tssfpp 2");   // set two flashes per pulse
            esper.useBackendTerminalCommand("tssfl 125");  // set flash lag to 0.5 mS
            esper.useBackendTerminalCommand("tssfe 1");    // enable flash output
            esper.useBackendTerminalCommand("tssop 1584"); // set camera output mask to GP5V - 1A, 1B, 2A, 2B
            //esper.useBackendTerminalCommand("tsen");       // enable triSync module
            esper.syncReadFPS().then((fps) => {
                console.log("API call - FPS read back: " + fps);
                setTimeout(() => {
                    // esper.useBackendTerminalCommand("tsog");          // capture free run
                    //esper.useBackendTerminalCommand("tsogn 10");      // capture 10 flashes
                    //esper.useBackendTerminalCommand("tsogms 1000");   // capture 1000 mS of TriSync Content
                    esper.disconnect();
                    process.exit(0);
                }, 500);
            }).catch(()=>{
                console.log("ERR: couldn't read triSync Frame Rate");
            })
            }, 2000);
        }, 500);

})
.catch((errs)=>{
    console.error("API::caught errors:");
    console.table(errs);
    console.log(errs);
});
