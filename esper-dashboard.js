const Esper = require('./esper-api');
let esper = new Esper();

let LIGHT_BAR_FLASH_DUR  = 8;
let MULTIFLASH_FLASH_DUR = 10;

let LIGHT_BAR_FLASH_INTENSITY  = 100;
let MULTIFLASH_FLASH_INTENSITY = 100;



esper.connect()
    .then(() => {
        console.log("Connected. Starting interactive mode...");
        setTimeout(() => {
            initUserInput();
        }, 700);
    })
    .catch((errs) => {
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });


function tearDownAndQuit() {
    process.stdin.setRawMode(false);
    esper.useBackendTerminalCommand("tscg");
    esper.disconnect();
    setTimeout(()=>{
            process.stdout.cursorTo(0,0, ()=>{
                process.stdout.clearScreenDown(()=>{
                    process.exit(0)
                });
            });
        },
    500);
}


function configureHybridRig() {
    let sequenceData = [];
    esper.useBackendTerminalCommand("r");

    setTimeout(()=>{
        esper.useBackendTerminalCommand("i");
        esper.getAvailableLights()
            .then((availableLights)=>{
                for (let i = 0; i < 32; i++){
                    let sequenceStage = [];
                    for (let light of availableLights){
                        let lsp = {
                            id:light.id,
                            duration: 0,
                            intensities: [0,0,0]
                        };

                        if (light.id <= 24){
                            lsp.duration = LIGHT_BAR_FLASH_DUR;
                            lsp.intensities = [LIGHT_BAR_FLASH_INTENSITY,LIGHT_BAR_FLASH_INTENSITY,LIGHT_BAR_FLASH_INTENSITY];
                        }
                        else if (light.id > 24 && light.id <= 28){
                            lsp.duration = MULTIFLASH_FLASH_DUR;
                            lsp.intensities = [MULTIFLASH_FLASH_INTENSITY,MULTIFLASH_FLASH_INTENSITY,MULTIFLASH_FLASH_INTENSITY];
                        }
                        else{
                            console.error("ERR: invalid light ID passed for this use case... quitting.");
                            tearDownAndQuit();
                        }
                        sequenceStage.push(lsp);
                    }
                    sequenceData.push(sequenceStage);
                }
                console.log("UPLOADING SEQUENCE DATA - CHECK BACKEND FOR PRINTOUT");
                return esper.sequence(sequenceData);
            })
            .catch((err)=>{
                console.log(err);
                console.table(err);
            });
    }, 500)
}


function configureInterleavedMode(){
    let sequenceData = [];
    esper.getAvailableLights().then((availableLights)=>{
        for (let i = 0; i < 6; i++){
            let sequenceStage = [];
            for (let light of availableLights){
                let lsp = {
                    id:light.id,
                    duration: 5,
                    intensities: [0,100,0]
                };
                sequenceStage.push(lsp);
            }
            sequenceData.push(sequenceStage);
        }

    let sequenceStage = [];
    for (let light of availableLights){
        let lsp = {
            id:light.id,
            duration: 0.1,
            intensities: [100,0,0]
        };
        sequenceStage.push(lsp);
    }
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    sequenceData.push(sequenceStage);
    esper.sequence(sequenceData).catch((err)=>{console.table(err)});
    esper.useBackendTerminalCommand("slas 10");
    esper.useBackendTerminalCommand("slts 7");

    })
}

function initUserInput() {
    require("readline").emitKeypressEvents(process.stdin);
    process.stdin.setRawMode(true);
    process.stdin.on("keypress", (err, key) => {
        let char = key.sequence;
        switch (char) {
            case "t":

                esper.useBackendTerminalCommand("tscg", ()=>{
                    esper.useBackendTerminalCommand("tsen");
                    esper.useBackendTerminalCommand("tssfpp 2");
                    esper.useBackendTerminalCommand("tssfl 2");
                    esper.useBackendTerminalCommand("tssfe 1");
                    esper.useBackendTerminalCommand("tssop 32");
                    esper.useBackendTerminalCommand("tsrpfps");
                });
                break;

            case "I":
                configureInterleavedMode();
                break;

           case "r":
                console.log("Resetting Lights...");
                esper.useBackendTerminalCommand("r");
                break;


            case "f":
                esper.testFlash().catch((err)=>{console.log("TestFlash() error: " + err)});
                break;

            case "k":
                console.log("Rewinding Sequence Point");
                esper.rewindSequencePoint().catch((err)=>{console.log("Rewind() error: " + err)});
                break;

            case "l":
                esper.globalModellingLight([0.2, 0.2, 0.2]).catch((err)=>{
                    console.log("ModellingLight() error: " + err);
                });
                break;

            case "o":
                esper.globalModellingLight([0.0, 0.0, 0.0]).catch((err)=>{
                    console.log("ModellingLight() error: " + err);
                });
                break;

            case "M":
                esper.useBackendTerminalCommand("tsen");
                esper.useBackendTerminalCommand("tsog");
                break;

            case "m":
                esper.useBackendTerminalCommand("tscg");
                esper.useBackendTerminalCommand("tsdis");
                break;

            case "A":
                console.log("awefawef");
                esper.useBackendTerminalCommand("f1", ()=>{
                    console.log("Camera focus ACK");
                });
                break;

            case "a":
                esper.useBackendTerminalCommand("f0", ()=>{
                    console.log("Camera focus ACK");
                });

                break;

            case "H":
                esper.useBackendTerminalCommand("f1");
                setTimeout(()=>{
                    esper.useBackendTerminalCommand("tsen");
                    esper.useBackendTerminalCommand("tsog");
                }, 100);
                break;


            case "h":
                esper.useBackendTerminalCommand("f0");
                setTimeout(()=>{
                    esper.useBackendTerminalCommand("tscg");
                    esper.useBackendTerminalCommand("tsdis");
                }, 100);
                break;


            case "D":
                esper.useBackendTerminalCommand("sdur 100 3");
                break;


            case ("c"):
                configureHybridRig();
                break;

            case " ":
                esper.useBackendTerminalCommand("tscg");
                esper.useBackendTerminalCommand("tsdis");
                esper.useBackendTerminalCommand("f1");
                const shutterCapDur = 500;
                setTimeout(()=> {
                    esper.useBackendTerminalCommand("k");
                    setTimeout(()=>{
                        esper.useBackendTerminalCommand("sdur " + shutterCapDur);
                        setTimeout(()=>{
                            esper.useBackendTerminalCommand("f0");
                            esper.useBackendTerminalCommand("tsen");
                            esper.useBackendTerminalCommand("tsog");
                        }, shutterCapDur + 500);
                    }, 30);
                }, 10);

                break;

            case "q":
                tearDownAndQuit();
        }
    });
}
