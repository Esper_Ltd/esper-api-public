const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

const PRE_FOCUS_MILLIS = 500;

esper.connect()
    .then(()=>{
        console.log("Requesting Focus...")
        // return new Promise((res, rej)=>{
            esper.useBackendTerminalCommand("f1", ()=>{
                console.log("Focus confirmed.");
            });
        // });
    })
    .then(()=>{
        console.log("Requesting wait...");
        return esper.wait(PRE_FOCUS_MILLIS)
    })
    .then(()=>{
        console.log("Asserting shutter line...");
        return esper.useBackendTerminalCommand("s 1");
    })
    .then(()=>{
        console.log("Wait...");
        return esper.wait(300);
    })
    .then(()=>{
        console.log("De-asserting shutter line...");
        return esper.useBackendTerminalCommand("s 0");
    })
    .then(()=>{
        console.log("Requesting release focus...")
        esper.useBackendTerminalCommand("f0", ()=>{
            console.log("Camera release focus ACK");
        });
    })
    .then(()=>{
        console.log("Post roll before disconnect");
        return esper.wait(PRE_FOCUS_MILLIS)
    })
    .then(()=>{
        setTimeout(()=>{
            console.log("Calling disconnect.");
            esper.disconnect();
        }, 500);
    })
    .then(()=>{
        return esper.disconnect();
}).catch((errs)=>{
    console.error("API::caught errors:");
    console.table(errs);
});

