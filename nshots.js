const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

if (process.argv.length === 3){
    esper.connect()
        .then(()=>{
            setTimeout(()=>{
                 esper.disconnect();
            }, 500);
            return esper.trigger({
                start:0,
                stages:Number.parseInt(process.argv[2]),
                fps: 10,
                preFocusDelay: 1000
            })
        })
    .catch((errs)=>{
        if (errs){
        console.error("API::caught errors:");
        console.table(errs);
        }
    });
}
else{
    console.log("ERR: Need exactly one argument: Num stages to capture");
}

