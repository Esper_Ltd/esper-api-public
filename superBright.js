const Esper = require('./esper-api');
let esper = new Esper();

//config for the script below
let dslrFlashDuration = 5;
let mvFlashDuration = 0.3;

//places to store the various sequences and stages
let dslrSequence = []
let mvLightingStage = [];



//connect to the api and
esper.connect()
    .then(()=>{

    //place to store the actual sequence to upload to the lights
    let sequenceData = [];

    //get all lights then create a lighting stage with each of them
    esper.getAvailableLights().then((availableLights)=>{

        //create a lighting sequence for the dslr capture
        for (let i = 0; i < 6; i++){
            let sequenceStage = [];
            for (let light of availableLights){
                let lsp = {
                    id:light.id,
                    duration: dslrFlashDuration,
                    intensities: [0,100,0]
                };
                sequenceStage.push(lsp);
            }
            dslrSequence.push(sequenceStage);
        }


        //create a single lighting stage for the mv that will be repeated ad-infinitum
        for (let light of availableLights){
            let lsp = {
                id:light.id,
                duration: mvFlashDuration,
                intensities: [100,0,0]
            };
            mvLightingStage.push(lsp);
        }


        //populate the first part of the sequence data with the dlsr sequnce
        sequenceData = [...dslrSequence];

        //spam in a bunch of mv stages
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);
        sequenceData.push(mvLightingStage);

        //upload the sequence to the dome
        esper.sequence(sequenceData).catch((err)=>{console.table(err)});

        //work out the loop at and loop to stages for the mv - based off the position of the end of the dslr sequence in the array
        let loopTo = dslrSequence.length  + 1;
        let loopAt = loopTo + 3;

        //configure the bits of the lighting stage to loop at
        esper.setLoopAtStage(loopAt);
        esper.setLoopToStage(loopTo);

    })
})
