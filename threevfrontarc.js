const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

esper.connect()
    .then(()=>{
esper.useBackendTerminalCommand("o");
        esper.useBackendTerminalCommand("l 257 100 1");
	esper.useBackendTerminalCommand("l 227 100 1");
	esper.useBackendTerminalCommand("l 197 100 1");
	esper.useBackendTerminalCommand("l 167 66 1");
	esper.useBackendTerminalCommand("l 137 66 1");
	esper.useBackendTerminalCommand("l 107 66 1");
	esper.useBackendTerminalCommand("l 77 100 1");
	esper.useBackendTerminalCommand("l 52 100 1");
	esper.useBackendTerminalCommand("l 17 0 1");
	esper.useBackendTerminalCommand("l 32 100 1");
	esper.useBackendTerminalCommand("l 7 100 1");
	esper.useBackendTerminalCommand("l 1 100 1");
	esper.useBackendTerminalCommand("l 2 100 1");
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });
