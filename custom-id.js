const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

/*
  * SET LWS IN C.S. to 1-146
 */
let idString = "1-82,101,121,139,140,122,123,102,83-100,103-120,124-138,141-146"; // CRASH
let idStringSBC = idString.split(",");
let ids = [];
for (let chunk of idStringSBC){
    if (chunk.includes("-")){
        let start = Number.parseInt(chunk.split("-")[0]);
        let finish = Number.parseInt(chunk.split("-")[1]);
        for (let i = start; i <= finish; i++){
            ids.push(i)
        }
    }
    else{
        ids.push(Number.parseInt(chunk));
    }
}

for (let id of ids){
    console.log("ID: " + id);
}

function uploadIDs(){
    (async ()=>{
        for (let id of ids){
            let commandString = "legid 0 " + id;
                esper.useBackendTerminalCommand(commandString);
                await esper.wait(300);
                esper.useBackendTerminalCommand("l " + id);
                await esper.wait(100);
        }
    })();
}

esper.connect()
    .then(()=> {
        esper.useBackendTerminalCommand("r");
        setTimeout(()=>{
            uploadIDs();
        }, 1000);
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
    });

