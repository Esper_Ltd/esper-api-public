let EsperFactory = require("./esper-api.js");
let esper = new EsperFactory();

// This script demonstrates using the API to set the MultiFlash modelling light level in constant mode, according to the
// spherical gradient calculations used to generate directional light.

// NOTE:  The gradient is not obvious when looking at the lights' LEDs directly, only once a subject is illuminated does
// a gradient become apparent.

// Adjust the following two variables before calling the script from the command line:

// Set the vector used to light the subject
// [ 1, 0, 0] / [-1,  0,  0] - for left/right
// [ 0, 1, 0] / [ 0, -1,  0] - for top / bottom
// [ 0, 0, 1] / [ 0,  0, -1] - for front/back
let modellingLightDirection = [1,0,0];

// Set the maximum brightness of the MultiFlashes (3% limit)
let intensity_pc = 3.0;


esper.connect().then(()=>{
        return esper.getAvailableLights();
    })
    .then((availableLights)=>{
    console.table(availableLights);
        console.log("connected")
         console.table(availableLights);
        for (let light of availableLights){

            let interaction = calcUnitaryInteractionWith(light.positions, modellingLightDirection);
            esper.individualModellingLight({
                id : light.id,
                intensities : [0, (intensity_pc*interaction) > 3.0 ? 3.0 : (intensity_pc*interaction), 0]
            })
        }
    })
    .then(()=>{
        //esper.disconnect();
    })
.catch((err)=>{
    esper.describeErrors(err);
})

function dotProduct(a, b){
    if (a !== null && b !== null){
        return (a[0] * b[0]) + (a[1] * b[1]) + (a[2] * b[2]);
    }
    return null;
}

function calcUnitaryInteractionWith(vect1, vect2) {
    if (Array.isArray(vect1) && Array.isArray(vect2)){
        let dot = dotProduct(vect1, vect2);
        let magThis = calcMagnitude(vect1);
        let magThat = calcMagnitude(vect2);
        let unityInteraction = dot / (magThis * magThat);
        unityInteraction += 1;
        unityInteraction /= 2;
        if (!(unityInteraction >= 0 && unityInteraction <= 1)) {
            console.error("Unitary interaction calculation error:" + unityInteraction);
            return null;
        }
        return unityInteraction;
    }
    else{
        console.error("TypeError on calcUnitaryInteractionWith()");
        return null;
    }
}

/**
 *
 * @param {number[]} vectIn
 * @returns {number}
 */
function calcMagnitude(vectIn){
    return Math.sqrt(
        Math.pow(vectIn[0],2) +
        Math.pow(vectIn[1],2) +
        Math.pow(vectIn[2],2)
    );
}
