const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

const PRE_FOCUS_MILLIS = 500;
const REPEATS          = 5;
const DELAY_SECONDS    = 3;

async function doCapture(){
    console.log("Capturing...");
    await esper.useBackendTerminalCommand_async("s 1");
    await esper.wait(150);
    await esper.useBackendTerminalCommand_async("s 0");
    console.log("Capture complete.");
    await esper.wait(150);
}

esper.connect()
    .then(async ()=>{
        await esper.useBackendTerminalCommand_async("f1"); // focus cameras
        await esper.wait(PRE_FOCUS_MILLIS);
        await esper.useBackendTerminalCommand_async("k"); // Rewinds the lights before capture
        for (let i = 0; i < REPEATS; i++){
            await doCapture();
            await esper.wait(DELAY_SECONDS * 1000);
        }
        await esper.useBackendTerminalCommand_async("f0"); // un focus cameras
        await esper.useBackendTerminalCommand_async("k"); // Rewinds the lights after capture
    })
    .then(()=>{
        esper.useBackendTerminalCommand_async("s 0"); // Ensure camera shutters aren't held down
        return esper.disconnect();
    }).catch((errs)=>{
    esper.useBackendTerminalCommand_async("s 0"); // Really ensure camera shutters aren't held down
    console.error("API::caught errors:");
    console.table(errs);
});

