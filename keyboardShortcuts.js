const EsperApi = require('./esper-api');
let esperApi = new EsperApi();

initUserInput();

esperApi.connect().then(()=>{
    promptUser("");
})

let commands = [
    "l - lights low",
    "o - lights off",
    "k - reset to seq point zero",
    "r - reset lights and clear upload",
    "i - Load IDs / Addresses",
]

function refreshPrompt(){
    process.stdout.cursorTo(0,0, ()=>{
        process.stdout.clearScreenDown(()=>{
            process.stdout.clearLine(()=>{
                for (let c of commands){
                    process.stdout.write(c);
                    process.stdout.write("\n");
                }
            })
        });
    })
}


function promptUser(arg){
    process.stdout.write(arg + " >> ");
}

process.stdin.on("keypress", (err, key) => {
    let char = key.sequence;
    switch (char){
        case "f":
            esperApi.testFlash();
            break;

        case "k":
            esperApi.setCurrentSequencePoint(0);
            break;

        case "l":
            esperApi.globalModellingLight([0.15, 0.15, 0.15]);
            break;

        case "o":
            esperApi.globalModellingLight([0,0,0]);
            break;

        case "i":
            esperApi.useBackendTerminalCommand("i");
            break;

        case "r":
            esperApi.useBackendTerminalCommand("r");
            break;

        case "x":
            esperApi.disconnect();
            process.exit(0);
            break;
    }
    setTimeout(()=>{
        refreshPrompt();
    }, 500);
});

function initUserInput(){
    try{
        require("readline").emitKeypressEvents(process.stdin);
        process.stdin.setRawMode(true);
        process.stdout.cursorTo(0,0, ()=>{
            process.stdout.clearScreenDown(()=>{
                process.stdout.write("Started CLI applet. Attempting to connect to controlSuite...");
            });
        })
    }
    catch(err){
        console.error("Couldn't initialise user input... further prompts will not respond to keypresses.")
    }
}
