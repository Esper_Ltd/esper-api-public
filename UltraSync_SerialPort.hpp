#ifndef SERIALTEST_ULTRASYNC_SERIALPORT_HPP
#define SERIALTEST_ULTRASYNC_SERIALPORT_HPP
#include <thread>
#include <chrono>
#include <sstream>
#include <windows.h>
#include <mutex>

class UltraSync_SerialPort{
#define BUFFER_SIZE 14
    HANDLE serialHandle;
    uint8_t bufferIndexIn;

    uint8_t bufferIn [BUFFER_SIZE];
    std::thread serialThread;
public:
    int hours = -1;
    int minutes = -1;
    int seconds = -1;
    int frames = -1;
    bool error = false;

    UltraSync_SerialPort(const std::string portName){
        std::string path = "\\\\.\\";
        path.append(portName);
        serialHandle = CreateFile(path.c_str(), GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
        bufferIndexIn = 0;

        // Do some basic settings
        DCB serialParams = { 0 };
        serialParams.DCBlength = sizeof(serialParams);

        GetCommState(serialHandle, &serialParams);
        serialParams.BaudRate = 57600;
        serialParams.ByteSize = 8;
        serialParams.StopBits = ONESTOPBIT;
        serialParams.Parity = NOPARITY;
        SetCommState(serialHandle, &serialParams);

        // Set timeouts
        COMMTIMEOUTS timeout = { 0 };
        timeout.ReadIntervalTimeout = 50;
        timeout.ReadTotalTimeoutConstant = 50;
        timeout.ReadTotalTimeoutMultiplier = 50;
        timeout.WriteTotalTimeoutConstant = 50;
        timeout.WriteTotalTimeoutMultiplier = 10;
        SetCommTimeouts(serialHandle, &timeout);
    }

    uint8_t getNextByte(){
        unsigned long numRead = 0;
        unsigned char byteIn;
        if (ReadFile(serialHandle, &byteIn, 1, &numRead, NULL)){
            return byteIn;
        }
        else{
            std::cout << "ERR: couldn't read from serial port" << std::endl;
            while (1){
                this->error = true;
            }
            return 0;
        }
    }

    std::string getLine(){
        while (this->getNextByte() != '#'){}
        this->bufferIn[0] = '#';
        this->bufferIndexIn = 1;
        while ( bufferIndexIn < BUFFER_SIZE ){
            this->bufferIn[this->bufferIndexIn] = this->getNextByte();
            this->bufferIndexIn++;
        }

        std::stringstream hoursString;
        hoursString << this->bufferIn[6];
        hoursString << this->bufferIn[7];

        std::stringstream minutesString;
        minutesString << this->bufferIn[8];
        minutesString << this->bufferIn[9];

        std::stringstream secondsString;
        secondsString << this->bufferIn[10];
        secondsString << this->bufferIn[11];

        std::stringstream framesString;
        framesString << this->bufferIn[12];
        framesString << this->bufferIn[13];

        this->hours = atoi(hoursString.str().c_str());
        this->minutes = atoi(minutesString.str().c_str());
        this->seconds = atoi(secondsString.str().c_str());
        this->frames = atoi(framesString.str().c_str());
    }

    void begin(){
        serialThread = std::thread ([&](){
            while (1){
                this->getLine();
            }
        });
    }
};
#endif //SERIALTEST_ULTRASYNC_SERIALPORT_HPP
