const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

esper.connect()
    .then(()=>{
        let lightSequence = [];
        for (let i = 0; i < 5; i++){
            lightSequence.push([
                {id: 1, intensities: [50, 50, 50], duration: 1},
                {id: 2, intensities: [50, 50, 50], duration: 1}
            ]);
        }
        esper.sequence(lightSequence)
        esper.sequence(lightSequence)
        esper.sequence(lightSequence)
        esper.sequence(lightSequence)
        esper.sequence(lightSequence)
        esper.sequence(lightSequence)
        return esper.tellMeWhenItsDone();
    })
    .then(()=>{
        console.log("NEXT TASK");
        esper.disconnect();
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });
