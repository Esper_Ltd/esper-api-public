const EsperFactory = require('./esper-api');
let esper = new EsperFactory();
esper.connect()
    .then(()=>{
        return esper.getAvailableLights();
    })
    .then((lightList)=>{
        for (let light of lightList){
            if (light.isHubNode){
                esper.useBackendTerminalCommand("r " + light.id);
            }
        }
        esper.useBackendTerminalCommand("i");
        esper.disconnect();
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });
