const rl = require("readline")
const Esper = require('./esper-api');
let esper = new Esper();


//connect to the api and
esper.connect()
    .then(()=>{

        console.log("press 'b' to begin");
        console.log("press 's' to stop");
        console.log("press 'q' to quit script");

        rl.emitKeypressEvents(process.stdin);
        process.stdin.setRawMode(true);
        process.stdin.on("keypress", (err, key) => {
            let char = key.sequence;
            switch (char) {

                case "b":
                    esper.startMvDrivenCapture()
                    break;

                case "s":
                    esper.stopMVDrivenCapture();
                    break;

                case "q":
                    process.stdin.setRawMode(false);
                    esper.stopMVDrivenCapture();
                    esper.disconnect();
                    setTimeout(()=>{
                            process.stdout.cursorTo(0,0, ()=>{
                                process.stdout.clearScreenDown(()=>{
                                    process.exit(0)
                                });
                            });
                        },
                        500);
                    esper.disconnect();
            }
        });
})



