const EsperFactory = require('./esper-api');
let esper = new EsperFactory();


// USER CONFIG PARAMS:
const olatBrightnesses = [5,5,5];    // max [100, 100, 100]
const giBrightnesses = [0, 10, 0];  // max [100, 100, 100]
const olatDuration_ms = 10;          // max 30
const giDuration_ms = 1;             // max 30
const olat_gi_stride = 10;           
const num_lights = 156;

esper.connect()
    .then(()=>{
        return esper.getAvailableLights();
    })
    .then(async (availableLights) => {
        let interleavedSequence = [];
        for (let i = 0; i < olat_gi_stride; i++) {
            let thisStage = [];
            for (let light of availableLights) {
                thisStage.push({
                    id: light.id,
                    intensities: i === (olat_gi_stride - 1) ? giBrightnesses : [0,0,0],
                    duration: giDuration_ms
                })
            }
            interleavedSequence.push(thisStage);
        }
        await esper.sequence(interleavedSequence);
        console.log("Sequence Uploaded, configuring OLAT mode...")
        let olatPayload = [];
        for (let light of availableLights){
            olatPayload.push({
                id: light.id,
                flashPosition: Math.round((light.id-1)/2),
                duration: olatDuration_ms,
            });
        }
        await esper.configureChainMode(olatPayload);
        await esper.armChainMode(olatBrightnesses);
        console.log("OLAT CONFIGURED;")

        for (let light of availableLights){
            if ((light.id % 2) !== 0){
                esper.useBackendTerminalCommand("m-id " + light.id + " 3");
                esper.useBackendTerminalCommand("slas-id " + light.id + " " + num_lights);
                esper.useBackendTerminalCommand("slts-id " + light.id + " " + 0);
            }
            else{
                esper.useBackendTerminalCommand("m-id " + light.id + " 1");
                esper.useBackendTerminalCommand("slas-id " + light.id + " " + olat_gi_stride);
                esper.useBackendTerminalCommand("slts-id " + light.id + " " + 0);

            }
        }
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });
