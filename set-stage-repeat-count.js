const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

let args = process.argv; args.shift(); args.shift();

if (args.length < 1){
    console.log("ERR: user must specify stage repeat arg - e.g:");
    console.log("$ node set-stage-repeat-count.js 3");
    process.exit(-3);
}

let repeats = 0;
try {
    repeats = Number.parseInt(args[0]);
    if (repeats > -1 && repeats < 256){
        console.log("Setting repeat counter to " + repeats);
    }
    else{
        console.log("ERR: repeats value must be in range 0->255");
        process.exit(-10);
    }
}
catch (err){
    console.log("Parse stage repeats arg err:");
    console.log(err);
    process.exit(-1);
}

esper.connect()
    .then(()=> {
        setTimeout(()=>{
            process.exit(0);
        }, 500);
        return esper.setSequenceStagePlaybackCount(repeats);
    })
    .then(()=>{
        return esper.disconnect();
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
    });

