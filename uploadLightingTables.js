let EsperClass = require("./esper-api.js");
let esper = new EsperClass(true); // enable verbose reporting

let userIntensities    = [0, 100, 0];
let userDuration_ms    = 0.2;

esper.connect()
    .then(()=>{
        return esper.getAvailableLights();
    })
    .then((lightsList) => {
        esper.useBackendTerminalCommand("PT 0");
        let stage = [];
        for (let i = 0; i < lightsList.length; i++){
            console.log(lightsList[i]);
            stage.push(
                {id: lightsList[i].id, intensities: userIntensities, duration: userDuration_ms}
            );
        }
        let payload = [];
        for (let j = 0; j < 32; j++){
            payload.push(stage);
        }
        return esper.sequence(payload);
    })
    .then(()=>{
        esper.useBackendTerminalCommand("slas 31");
        esper.useBackendTerminalCommand("slts 0");
    })

    .then(() => {
        esper.disconnect();
    })

    .catch((errs) => {
    esper.describeErrors(errs);
});
