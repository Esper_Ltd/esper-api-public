const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

esper.connect().then(()=>{
    setTimeout(()=> {
        esper.useBackendTerminalCommand("PT 0");       // disable simple pass-thru from input jack.
        esper.useBackendTerminalCommand("k");          // rewind all lights to sequence point zero.
        setTimeout(()=>{
            esper.useBackendTerminalCommand("tsen");
            esper.useBackendTerminalCommand("tssfl 125");  // set flash lag to 0.5 mS
            esper.useBackendTerminalCommand("tssfe 1");    // enable flash output
            esper.syncReadFPS().then((fps) => {
                console.log("API call - FPS read back: " + fps);
                setTimeout(() => {
                    // esper.useBackendTerminalCommand("tsog");          // capture free run
                    esper.useBackendTerminalCommand("tsogn 160");      // capture 160 flashes
                    // esper.useBackendTerminalCommand("tsogms 1000");   // capture 1000 mS of TriSync Data
                    esper.disconnect();
                    process.exit(0);
                }, 500);
            }).catch(()=>{
                console.log("ERR: couldn't read triSync Frame Rate");
            })
            }, 2000);
        }, 500);

})
.catch((errs)=>{
    console.error("API::caught errors:");
    console.table(errs);
    console.log(errs);
});
