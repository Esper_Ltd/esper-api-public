const EsperApi = require('./esper-api');
let esperApi = new EsperApi();

// define the lighting directions  (change to match your direction convention if needed..)
const DIR_VECT_GLOBAL    = [ 0, 0, 0];

// define led names: (don't change!)
const NEUTRAL_LED    = 1;
const CROSS_LED      = 0;
const PARALLEL_LED   = 2;

// specify which directions to upload in sequence order: (This array length must match ledsToUse.length)
let directionsToUse = [
    DIR_VECT_GLOBAL,
    DIR_VECT_GLOBAL,
    DIR_VECT_GLOBAL,];

// specify which leds to use in sequence order:
const ledsToUse = [
    NEUTRAL_LED,
    CROSS_LED,
    PARALLEL_LED,
];

const perStageIntensityMask = [
    1,
    1,
    1,
];

const fDur = 5;

const perStageFlashDuration = [
    0.4 * fDur,
    1   * fDur,
    0.7 * fDur
];

let activeLightIDs = [
    //155,
    146, 133,
    127, 129, 131, 113, 115, 117,
    87, 89, 91, 73, 75, 77,
    47, 49, 51, 33, 35, 37,
    15, 7, 9
]; // 24

activeLightIDs.push(91);
activeLightIDs.push(73);

activeLightIDs.push(70);
activeLightIDs.push(53);

activeLightIDs.push(93);
activeLightIDs.push(110);

console.log(activeLightIDs.length);


if (ledsToUse.length === directionsToUse.length && ledsToUse.length === perStageIntensityMask.length){
    esperApi.connect()
        .then(() => {
            let lights = JSON.parse(JSON.stringify(esperApi.availableLights)); // lazy deep copy.

            for (let light of lights){
                light.intensitiesTable = [];
                light.ledsTable = [];
                for (let sIt = 0; sIt < directionsToUse.length; sIt++){
                    let calculatedIntensity_Unitary = -1;
                    if (activeLightIDs.includes(light.id)){
                        calculatedIntensity_Unitary = 1; // GI
                        //esperApi.individualModellingLight({id: light.id, intensities:[1,1,1]});
                    }
                    else{
                        calculatedIntensity_Unitary = 0;
                    }
                    // console.log("light.id")
                    light.intensitiesTable.push(calculatedIntensity_Unitary * 100 * perStageIntensityMask[sIt]);
                    light.ledsTable.push(ledsToUse[sIt]);
                }
            }

            let sequenceHolder = [];
            for (let stageIndex = 0; stageIndex < directionsToUse.length; stageIndex++){
                let thisStageArray = [];
                for (let light of lights){
                    let intensitiesTrio = [0,0,0];
                    intensitiesTrio[light.ledsTable[stageIndex]] = light.intensitiesTable[stageIndex];
                    thisStageArray.push({
                        id: light.id,
                        intensities: intensitiesTrio,
                        duration: perStageFlashDuration[stageIndex]
                    })
                }
                sequenceHolder.push(thisStageArray);
            }
            return esperApi.sequence(sequenceHolder);
        })

    .catch((err)=>{
        console.table(err);
    });
}
else{
    console.error("ERR: directions array and leds array must be of equal length.")
}

function getLightWithID(lightsList, id){
    for (let light of lightsList){
        if (light.id === id){
            return light;
        }
    }
    console.error("ERR: Can't find light with ID!!");
}

