let EsperFactory = require("./esper-api.js");
let esper = new EsperFactory();

/* #### USER SETTING SECTION START  #### */

// let lightShape = "SINGLE";
let lightShape = "BLOB";

let polarisationState = "CROSS";
// let polarisationState = "PARALLEL";
// let polarisationState = "NEUTRAL";

let flashDuration_mS = 5;
let numStages = 8;
let lightingSequence = [];

let VERBOSE = true;

/*  #### USER SETTING SECTION END #### */
const domeRadius = 1200;


esper.connect().then(()=>{
    return esper.getAvailableLights();
})
.then((availableLights)=>{
    for (let i = 0; i < numStages; i++){
        let polarAngle = 2*Math.PI * (i / numStages);
        let lightPosition = [Math.round(Math.sin(polarAngle) * domeRadius),
                             0,
                             Math.round(Math.cos(polarAngle) * domeRadius)
                            ];
        let centerLightID = getIDClosestToPosition(lightPosition, availableLights);
        console.log("Building SequenceStage for centreID: " + centerLightID);
        let blobIDs = [];
        if (lightShape === "BLOB"){
            blobIDs = findNLightsNearID(centerLightID, 6, availableLights);
        }

        let sequenceStage = [];
        for (let light of availableLights){
            let intensityArray = [0,0,0];
            if (light.id === centerLightID || blobIDs.includes(light.id)){
                intensityArray[0] = 100;
                intensityArray[1] = 100;
                intensityArray[2] = 100;
            }
            sequenceStage.push({
                id: light.id,
                intensities: intensityArray,
                duration: flashDuration_mS
            });
        }
        if (VERBOSE){
            console.table(sequenceStage);
        }
        lightingSequence.push(sequenceStage);
    }


    return esper.sequence(lightingSequence);
})
    .then(()=>{
        console.log("Uploaded...");
    })



////////////////////////// HELPER FUNCTIONS ////////////////////////////////////////

function __pass(){

}

function getIDClosestToPosition(positionArray, lightList){
    let leastDistance = 9999999999999999;
    let nearestID = -1;
    for (let light of lightList){
        let distance = calcDistance(light.positions, positionArray);
        if (distance < leastDistance){
            nearestID = light.id;
            leastDistance = distance;
        }
    }
    if (nearestID == -1){
        throw("LightNotFoundException");
    }
    return nearestID;
}

function calcDistance(arr1, arr2){
    if (Array.isArray(arr1) && Array.isArray(arr2)){
        return Math.sqrt(
          Math.pow(arr1[0]-arr2[0], 2) +
          Math.pow(arr1[1]-arr2[1], 2) +
          Math.pow(arr1[2]-arr2[2], 2)
        );
    }
    else{ throw ("NotAnArrayError");}
}


function r2d(rad){
    return (180/Math.PI) * rad;
}

function getPositionFromID(id, searchList){
    for (let light of searchList){
        if (light.id === id){
            return light.positions;
        }
    }
    throw ("LightNotFoundException");
}

function findNLightsNearID(centerLightID, numLights, searchList){
    let foundIDs = [];
    while (foundIDs.length < numLights){
        let minDistance = 1000000;
        let minValidID = -1;
        for (let light of searchList){
            let inspectionDistance = calcDistance(light.positions, getPositionFromID(centerLightID, searchList));
            if (inspectionDistance < minDistance){ // if this light is the closest so far
                if (light.id !== centerLightID){   // and this light is not the centre ID we're searching against
                    let isDuplicate = false;       // Check if the list already contains this light's ID
                    for (let afid of foundIDs){
                        if (light.id === afid){
                            isDuplicate = true;
                        }
                    }
                    if (! isDuplicate){
                        minDistance = inspectionDistance;
                        minValidID = light.id;
                    }
                }
            }
        }
        if (minValidID !== -1){
            foundIDs.push(minValidID);
        }
        else{
            throw("LightNotFoundException");
        }
    }
    return foundIDs;
}
