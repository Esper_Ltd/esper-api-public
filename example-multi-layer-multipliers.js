const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

let crossIntensity    = 0;
let neutralIntensity  = 100;
let parallelIntensity = 0;
let flashDuration_ms  = 5;

let modellingIntensity = 100;

let layerMultipliers = [
  1,  // a :: 1
  1,  // b :: 5
  1,  // c :: 10
  1,  // d :: 15
  1,  // e :: 20
  1,  // f :: 25
  1,  // g :: 30
  1,  // h :: 30
  1,  // i :: 30
  1,  // j :: 30
  1,  // k :: 30
  1,  // l :: 30
  1,  // m :: 30
  1,  // n /////  ?????
];

 /* let layerMultipliers = [
    0.8,  // a :: 1
    0.8,  // b :: 5
    0.8 ,  // c :: 10
    0.8,  // d :: 15
    0.5,  // e :: 20
    0.5,  // f :: 25
    0.5,  // g :: 30
    0.5,  // h :: 30
    0.3,  // i :: 30
    0.3,  // j :: 3
    0.3,  // k :: 30
    0.3,  // l :: 30
    0.3,  // m :: 30
    1,  // n /////  ?????
 ]; */

const ledArray = [crossIntensity, neutralIntensity, parallelIntensity];

esper.connect()
    .then(()=>{
        return esper.getAvailableLights();
    })
    .then((availableLights)=>{
        for (let light of availableLights){
            if (light.id >= 287){
                light.positionDescription = "n-12/12"
            }
        }

        if (process.argv.includes("--illuminate")){
            illuminateRig(availableLights);
        }
        else{
            return esper.sequence(buildMultiLayerSequence(availableLights));
        }
    })
    .then(()=>{
//        esper.disconnect();
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });


function buildMultiLayerSequence(availableLightsList){
    let multiLayerSequence = [];
    for (let stageIndex = 0; stageIndex < 32; stageIndex++){
        let thisStageArray = [];
        for (let light of availableLightsList){
            let layerIntensities = JSON.parse(JSON.stringify(ledArray));
            let layerIndex = light.positionDescription.split("-")[0].charCodeAt(0)-97;
            for (let i = 0; i < layerIntensities.length; i++){
                layerIntensities[i] *= layerMultipliers[layerIndex]
            }
            thisStageArray.push({
                id: light.id,
                intensities: layerIntensities,
                duration: flashDuration_ms
            });
        }
        multiLayerSequence.push(thisStageArray);
    }
    console.log(multiLayerSequence);
    return multiLayerSequence;
}

function illuminateRig(availableLightsList){
    esper.modellingLightOff();
    setTimeout(()=>{
        let layer = "all";
        if (process.argv.includes("--layer")){
            layer = process.argv[process.argv.indexOf("--layer") + 1];
        }
        for (let light of availableLightsList){
            let layerIndex = light.positionDescription.split("-")[0].charCodeAt(0)-97;
            let layerLetter = light.positionDescription.split("-")[0];
            let modellingLightIntensityArray = [modellingIntensity * layerMultipliers[layerIndex] * 0.05, modellingIntensity * layerMultipliers[layerIndex], modellingIntensity * layerMultipliers[layerIndex] * 0.05];
            if (layer === "all"){
                console.log("Setting modelling light: " + light.positionDescription);
                //let terminalCommand = "l " + light.id + " " + modellingLightIntensityArray[0] + " 0";
                //esper.useBackendTerminalCommand(terminalCommand);
                terminalCommand = "l " + light.id + " " + modellingLightIntensityArray[1] + " 1";
                esper.useBackendTerminalCommand(terminalCommand);
                //terminalCommand = "l " + light.id + " " + modellingLightIntensityArray[2] + " 2";
                //esper.useBackendTerminalCommand(terminalCommand);
            }
            else if (layer === layerLetter){
                console.log("layerLetter: " + layerLetter);
                console.log("user layerLetter: " + layer);
                console.log("Setting modelling light: " + light.positionDescription);
                console.log(modellingLightIntensityArray);
                console.log("LightID:" + light.id);
                let terminalCommand = "l " + light.id + " " + modellingLightIntensityArray[1] + " 1";
                esper.useBackendTerminalCommand(terminalCommand);
            }
        }
    }, 500);
}
