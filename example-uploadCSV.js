let EsperFactory = require("./esper-api.js");
let esper = new EsperFactory();

let spreadsheetFile = process.argv[2];
let fileReader = require("fs");


esper.connect().then(()=>{
    return esper.getAvailableLights()
})
    .then((availableLights)=>{
        let data = fileReader.readFileSync(spreadsheetFile, "utf8");
        console.log("Found " + availableLights.length + " lights...");
        console.log("Opening csv: " + spreadsheetFile);
        let lines = data.split("\n");
        let sequenceHolder = [];
        for (let stageIndex = 0; stageIndex < 32; stageIndex++) {
            let thisStageArray = [];
            for (let light of availableLights) {
                let flashDuration = Number.parseFloat(lines[light.id].split(",")[1]);
                if (flashDuration > 25){
                    console.error("ERR: flash duration over max allowed value of 25mS ");
                    process.exit(11);
                }
                let intensitiesTrio = [
                    Number.parseFloat(lines[light.id].split(",")[2 +     (stageIndex*3)]),
                    Number.parseFloat(lines[light.id].split(",")[2 + 1 + (stageIndex*3)]),
                    Number.parseFloat(lines[light.id].split(",")[2 + 2 + (stageIndex*3)]),
                ];
                thisStageArray.push({
                    id: light.id,
                    intensities: intensitiesTrio,
                    duration: flashDuration
                });
            }
            sequenceHolder.push(thisStageArray);
        }
        return esper.sequence(sequenceHolder);
    }).then(()=>{
        return esper.disconnect();
    })
    .catch((err)=>{
        console.log("ERR:" + err);
        return esper.disconnect();
});










