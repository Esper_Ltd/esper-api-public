let esperClass = require("./esper-api");
let Esper = new esperClass(true); // enable verbose reporting

let duration = 0.1;

Esper.connect()
    .then(()=>{
        //modelling light off
        return Esper.globalModellingLight([0, 0, 0]);
    })


    .then(() => {
        let lightSequence = [];
        for (let i = 0; i < 32; i++){
            lightSequence.push([
                {id: 1, intensities: [50, 50, 50], duration: 1},

                ]);
        }

        return Esper.sequence(lightSequence)
    })

    .then(()=>{
        console.log("triggering...");
        Esper.trigger({
            start:0,
            stages:10,
            fps: 10,
            preFocusDelay:400,
            captureMode: 'mv'})
    })

    .then(()=>{
        Esper.disconnect();
    })

    .catch((err)=>{
        Esper.describeErrors(err);
        Esper.disconnect();
    });
