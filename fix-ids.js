const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

esper.connect()
    .then(()=>{
        console.log("Resetting lights...");
        esper.useBackendTerminalCommand("r");
        console.log("Waiting for lights to boot up...")
        setTimeout(()=>{
            console.log("ID-ing first light...");
            esper.useBackendTerminalCommand("legid 0 1");
            setTimeout(()=>{
                console.log("ID-ing second light...");
                esper.useBackendTerminalCommand("legid 0 2");
                console.log("Turning lights off...");
                esper.useBackendTerminalCommand("o");
                setTimeout(()=>{
                    console.log("Directly illuminating two lights:");
                    esper.useBackendTerminalCommand("l 1");
                    esper.useBackendTerminalCommand("l 2");
                    console.log("### Check 2 lights at top of dome are illuminated ###")
                }, 500);
            })
        }, 3000);
    });

