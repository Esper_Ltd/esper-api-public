const EsperFactory = require('./esper-api');
let esper = new EsperFactory();

const insertionIndex = 165;
const newID          = 332;
const line           =   0;
const positionArray  = [0,0,1500];

esper.connect()
    .then(()=>{
        return esper.getAvailableLights();
    })
    .then((availableLights)=>{
        let duplicate = false;
        for (let light of availableLights){
            if (light.id === newID){
                console.log()
                console.log(`ControlSuite already has light ${newID} in chain, skipping...`);
                console.log()
                duplicate = true;
            }
        }
        if (!duplicate){
            console.log()
            console.log("Adding new light to chain...");
            console.log()
            esper.useBackendTerminalCommand(
                `aen ${insertionIndex} ${newID} ${line} {${positionArray[0]},${positionArray[1]},${positionArray[2]}}`);
        }
    })
    .then(()=>{
        esper.disconnect();
    })
    .catch((errs)=>{
        console.error("API::caught errors:");
        console.table(errs);
        console.log(errs);
    });
